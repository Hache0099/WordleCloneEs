class_name UIInputHandler
extends Node

var _letter_index := 0
var _letters := []

func _ready() -> void:
	assert(get_parent() as LetterContainer)
	set_process_input(false)


func _input(event: InputEvent) -> void:
	var e := event as InputEventKey
	if !e:
		return
	
	
	if e.is_pressed():
		var key := OS.get_scancode_string(e.scancode)
		var code := e.scancode
		
#		print(key)
#		print("index = %d" % _letter_index)

		var l := _letters[_letter_index] as LetterNode
		assert(l)
	
		if (code < 90 and code > 64) or key == "ñ":
			if l.is_empty():
				l.set_letter(key)
			if _letter_index < _letters.size() - 1:
				_letter_index += 1

		elif code == KEY_BACKSPACE:
			if !l.is_empty():
				l.set_letter(" ")
			else:
				if _letter_index > 0:
					_letter_index -= 1
				_letters[_letter_index].set_letter(" ")


func _on_LetterCont_ready() -> void:
	_letters = (get_parent() as LetterContainer).get_letter_nodes()
