extends Control

export(int,10) var containers_amount := 6
export(int,8) var letters_amount := 5
export(PackedScene) var letters : PackedScene

onready var container := $VBoxContainer/MarginContainer/VBoxContainer as VBoxContainer
var _current_letter_index := 0
var _current_letter_node : Control

func _ready() -> void:
	_spawn_letters()
	_set_current_letter(0)
	_current_letter_node.set_active(true)


func _spawn_letters() -> void:
	for i in containers_amount:
		var l := letters.instance()
		l.set_amount_letters(letters_amount)
		container.add_child(l)


func _set_current_letter(i : int) -> void:
	_current_letter_node = container.get_child(i)


func _input(event: InputEvent) -> void:
	var e := event as InputEventKey
	if !e:
		return
	
	if e.is_pressed() and e.scancode == KEY_ENTER:
		var word : String = _current_letter_node.get_word()
		if word.length() == letters_amount:
			if $WordLogic.check_word(word):
				print(word)
			else:
				print("Nope")
