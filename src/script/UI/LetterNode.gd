class_name LetterNode
extends ColorRect

onready var label := $Label as Label
const colors := {
	WordDb.Precision.CORRECTO: Color.greenyellow,
	WordDb.Precision.NO_LUGAR: Color.yellow,
	WordDb.Precision.CORRECTO: Color(272727),
}

func _ready() -> void:
	pass


func set_color(i: int) -> void:
	set_frame_color(colors[i])


func set_letter(s: String) -> void:
	assert(s.length() <= 1)
	label.set_text(s)


func get_letter() -> String:
	return label.get_text()


func is_empty() -> bool:
	return label.get_text().empty() or label.get_text() == " "
