class_name WordDb
extends Resource

enum Precision {CORRECTO, NO_LUGAR, INCORRECTO}

export(PoolStringArray) var palabras := PoolStringArray([])
export(String) var palabra_elegida := ""


func set_palabras(array : PoolStringArray) -> void:
	palabras = array


func set_palabra_elegida() -> void:
	palabra_elegida = get_palabra_aleatoria()


func get_palabra_elegida() -> String:
	return palabra_elegida


func get_palabra_aleatoria() -> String:
	return palabras[randi() % palabras.size()]


func get_correcto(word : String) -> Dictionary:
	var d := {}
	
	for i in word.length():
		var w := word[i] as String
		d[w] = -1
		
		if !(w in palabra_elegida):
			d[w] = Precision.INCORRECTO
			continue
		else:
			if w == palabra_elegida[i]:
				d[w] = Precision.CORRECTO
			else:
				d[w] = Precision.NO_LUGAR
	
	return d


func is_in_db(word : String) -> bool:
	return word in palabras
