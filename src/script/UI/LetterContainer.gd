class_name LetterContainer
extends Control

export(int,10) var amount_letters := 5
export(PackedScene) var letter_node : PackedScene

onready var Letters := $HBoxContainer 
onready var input := $UIInputHandler

func _ready() -> void:
	_spawn_letters()
	set_active(false)


func set_active(value: bool) -> void:
	input.set_process_input(value)


func set_amount_letters(a: int) -> void:
	amount_letters = a


func _spawn_letters() -> void:
	for i in amount_letters:
		var l : Control = letter_node.instance()
		l.rect_min_size = Vector2(340 / amount_letters - 5, 0)
		Letters.add_child(l)


func set_colors_letter(d: Dictionary) -> void:
	var val := d.values()
	var child := get_letter_nodes()
	for i in val.size():
		child[i].set_color(val[i])


func get_letter_nodes() -> Array:
	return Letters.get_children()


func get_word() -> String:
	var word := ""
	
	for child in Letters.get_children():
		var l : String = child.get_letter()
		if l != "" and l != " ":
			word += child.get_letter()
	
	return word
