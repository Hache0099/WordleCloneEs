extends Node

var word_db : WordDb = load("res://res/WordDb.tres") as WordDb


func _ready() -> void:
	randomize()
	set_palabra()


func check_word(s: String) -> bool:
	return word_db.is_in_db(s)


func set_palabra() -> void:
	word_db.set_palabra_elegida()


func compare_words(s: String) -> Dictionary:
	var d := word_db.get_correcto(s)
	
	return d
